package com.saxobank.reference.websocketapp;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class ReauthorizingTask implements Runnable {

    public interface ReauthorizeCallback {
        void onReauthorize(AtomicReference<OAuthToken> atomicOAuthToken) throws InterruptedException;
    }

    private final AtomicReference<OAuthToken> atomicOAuthToken;
    private final ReauthorizeCallback callback;
    private final Thread thread;
    private final AtomicBoolean stopping = new AtomicBoolean(false);

    public ReauthorizingTask(AtomicReference<OAuthToken> atomicOAuthToken, ReauthorizeCallback callback) {
        this.atomicOAuthToken = atomicOAuthToken;
        this.callback = callback;
        thread = new Thread(this);
        thread.start();
    }

    public void stop() {
        stopping.set(false);
        thread.interrupt();
    }

    public void run() {
        while ( ! stopping.get() ) {
            try{
                callback.onReauthorize(atomicOAuthToken);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
                System.out.println("ReauthorizingTask was interrupted");
            }
        }
    }
}
