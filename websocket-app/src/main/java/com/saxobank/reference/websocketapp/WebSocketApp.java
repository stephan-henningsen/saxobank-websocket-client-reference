package com.saxobank.reference.websocketapp;


import javax.websocket.CloseReason;
import javax.websocket.DeploymentException;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;


public class WebSocketApp implements WebSocketMessageHandler.OnMessageReceivedListener, WebSocketClientEndpoint.WebSocketClientEndpointListener, ReauthorizingTask.ReauthorizeCallback {


    private static final String WEBSOCKET_CONNECTION_URL = "wss://gateway.saxobank.com/sim/openapi/streamingws/connect";
    private static final String WEBSOCKET_AUTHORIZATION_URL = "https://gateway.saxobank.com/sim/openapi/streamingws/authorize";
    private static final String REST_PRICES_SUBSCRIPTIONS_URL = "https://gateway.saxobank.com/sim/openapi/trade/v1/prices/subscriptions";

    private static final String OVERRIDE_AUTH_TOKEN = "eyJhbGciOiJFUzI1NiIsIng1dCI6IkQ2QzA2MDAwMDcxNENDQTI5QkYxQTUyMzhDRUY1NkNENjRBMzExMTcifQ.eyJvYWEiOiI3Nzc3NyIsImlzcyI6Im9hIiwiYWlkIjoiMTA5IiwidWlkIjoibVF5TGNVYUJ1OFBoQWs5OXRHcXJaUT09IiwiY2lkIjoibVF5TGNVYUJ1OFBoQWs5OXRHcXJaUT09IiwiaXNhIjoiRmFsc2UiLCJ0aWQiOiIyMDAyIiwic2lkIjoiYTk2ZTE2MmVkZjhkNGNjMzhhN2M1MWE5MzE4MmE3OTIiLCJkZ2kiOiI4NCIsImV4cCI6IjE1MzkzMjI3NDMifQ.B_OZ6YXZPptgbz6cvUCwv7ERY4QiLI-MR4Y8C0ei1RfxQY1Mthhks6pipaiDiQB6D9gyvZF7_ETXPUGyXVlEQQ";


    public static void main(String[] args) {
        String overrideAuthToken = envOrDefault("AUTH_TOKEN", OVERRIDE_AUTH_TOKEN);
        String contextId = "ctx-id-" + UUID.randomUUID();
        String referenceId = "ref-id-" + UUID.randomUUID();

        WebSocketApp app = new WebSocketApp();
        app.run(overrideAuthToken, contextId, referenceId);
    }

    private static String envOrDefault(String env, String defaultValue) {
        String value = System.getenv(env);
        if (value != null && !value.isEmpty()) {
            return value;
        }
        return defaultValue;
    }


    private final CountDownLatch exitLatch = new CountDownLatch(1);

    private String contextId;

    private ReauthorizingTask reauthorizingTask;
    private AtomicReference<OAuthToken> atomicOAuthToken;

    private WebSocketClientEndpoint webSocketClientEndpoint;
    private PriceSubscriptionClient priceSubscriptionClient;
    private AuthorizationClient authorizationClient;

    private long lastReceivedMessageId = -1;
    private int reauthorizedCount = 0;


    private void run(String overrideAuthToken, String contextId, String referenceId) {
        System.out.println("Hello, World!");

        this.contextId = contextId;

        atomicOAuthToken = new AtomicReference<>(getAuthenticationToken(overrideAuthToken));
        authorizationClient = new AuthorizationClient(WEBSOCKET_AUTHORIZATION_URL);
        reauthorizingTask = new ReauthorizingTask(atomicOAuthToken, this);

        WebSocketMessageHandler webSocketMessageHandler = new WebSocketMessageHandler();
        webSocketMessageHandler.addOnMessageReceivedListener(this);

        webSocketClientEndpoint = new WebSocketClientEndpoint(WEBSOCKET_CONNECTION_URL, contextId, atomicOAuthToken, this, webSocketMessageHandler);
        priceSubscriptionClient = new PriceSubscriptionClient(REST_PRICES_SUBSCRIPTIONS_URL);

        try {
            priceSubscriptionClient.subscribe(atomicOAuthToken, contextId, referenceId, 15, "FxSpot");

            webSocketClientEndpoint.connect();

            System.out.println("Ready!");
            exitLatch.await(); // Blocking until exitLatch has reached zero, effective when the app finishes.
        } catch (Exception ex) {
            System.err.println("Oops!  " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            webSocketClientEndpoint.close();
            reauthorizingTask.stop();
        }

        System.out.println("Have a nice day!");
    }


    private OAuthToken getAuthenticationToken(String overrideAuthToken) {
        final OAuthToken oAuthToken;
        if (overrideAuthToken == null) {
            throw new IllegalArgumentException("AUTH_TOKEN not set!");
        } else {
            System.out.println("Using overridden authorization token.");
            oAuthToken = OAuthToken.create(overrideAuthToken, 1000 * 60 * 2, "initalized-token"); // Token expires in 2 minutes.
        }
        System.out.println("OAuthToken=" + Jsons.toString(oAuthToken));
        return oAuthToken;
    }


    @Override
    public void onMessage(Message message) {
        if (message.payloadFormat == PayloadFormat.Json) {
            lastReceivedMessageId = message.messageId;
            System.out.println(">>> onMessage():");
            System.out.println("    message=" + Jsons.toString(message));
            System.out.println("    payload=" + new String(message.payload));
        } else {
            throw new IllegalArgumentException("Unexpected PayloadFormat: payloadFormat=" + message.payloadFormat + ", referenceId=" + message.referenceId);
        }
    }

    @Override
    public void onHeartbeat(HeartbeatControlMessage[] controlMessages) {
        System.out.println("!!! onHeartbeat():");
        System.out.println("    controlMessage=" + Jsons.toString(controlMessages));
    }

    @Override
    public void onResetSubscription(ResetSubscriptionsControlMessage[] controlMessages) {
        System.out.println("!!! onResetSubscription():");
        System.out.println("    controlMessage=" + Jsons.toString(controlMessages));
        for (ResetSubscriptionsControlMessage controlMessage : controlMessages) {
            priceSubscriptionClient.unsubscribe(contextId, controlMessage.targetReferenceIds);
        }
    }

    @Override
    public void onDisconnect(DisconnectControlMessage[] controlMessages) {
        System.out.println("!!! onDisconnect():");
        System.out.println("   controlMessage=" + Jsons.toString(controlMessages));
        webSocketClientEndpoint.close();
    }


    @Override
    public void onWebSocketClientEndpointOpen() {
    }

    @Override
    public void onWebSocketClientEndpointClose(CloseReason closeReason) {
        exitLatch.countDown();
    }

    @Override
    public void onWebSocketClientEndpointError(Throwable thr) {
        thr.printStackTrace();
        System.out.println("Error occurred, attempting to reconnect...");
        try {
            webSocketClientEndpoint.connect(lastReceivedMessageId);
        } catch (IOException | DeploymentException ex) {
            throw new RuntimeException(ex);
        }
    }


    @Override
    public void onReauthorize(AtomicReference<OAuthToken> atomicOAuthToken) throws InterruptedException {
        OAuthToken currentOAuthToken = atomicOAuthToken.get();
        int delay = currentOAuthToken.expiresIn - 1000 * 60; // Refresh 1 minute before token expires.
        System.out.println("Authentication Token expires in " + (currentOAuthToken.expiresIn / 1000) + " seconds: " + Jsons.toString(currentOAuthToken));
        System.out.println("Reauthorizing in " + (delay / 1000) + " seconds ...");

        Thread.sleep(delay);

        reauthorizedCount++;
        System.out.println("Reauthorizing, count=" + reauthorizedCount + " ...");
        authorizationClient.reauthorize(atomicOAuthToken.get().accessToken, contextId);

        OAuthToken updatedOAuthToken = OAuthToken.create(currentOAuthToken.accessToken, currentOAuthToken.expiresIn, "reauthorized-count=" + reauthorizedCount);
        atomicOAuthToken.set(updatedOAuthToken);

        System.out.println("Successfully reauthorized: " + Jsons.toString(updatedOAuthToken));
    }
}

