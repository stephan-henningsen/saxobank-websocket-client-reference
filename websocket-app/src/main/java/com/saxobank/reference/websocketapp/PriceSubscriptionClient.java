package com.saxobank.reference.websocketapp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.concurrent.atomic.AtomicReference;

public class PriceSubscriptionClient {


    public interface PriceSubscriptionApi {

        @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
        class PriceSubscriptionRequest implements Serializable {

            public class Arguments implements Serializable {
                public int uic;
                public String assetType;

                protected Arguments() {
                }

                public Arguments(int uic, String assetType) {
                    this.uic = uic;
                    this.assetType = assetType;
                }
            }

            public String contextId;
            public String referenceId;
            public PriceSubscriptionRequest.Arguments arguments;

            protected PriceSubscriptionRequest() {
            }

            public PriceSubscriptionRequest(String contextId, String referenceId, int uic, String assetType) {
                this.contextId = contextId;
                this.referenceId = referenceId;
                this.arguments = new Arguments(uic, assetType);
            }
        }


        @JsonIgnoreProperties(ignoreUnknown = true)
        @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
        class PriceSubscriptionResponse implements Serializable {

            // Note that only part of the response model has been implemented here.
            public String contextId;
            public String referenceId;
            public String format;

        }
    }


    private final URI subscriptionUri;

    public PriceSubscriptionClient(String priceSubscriptionUrl) {
        subscriptionUri = URI.create(priceSubscriptionUrl);
    }

    public PriceSubscriptionApi.PriceSubscriptionResponse subscribe(AtomicReference<OAuthToken> atomicOAuthToken, String contextId, String referenceId, int uic, String assetType) throws IOException {
        System.out.println("subscribe() ...");

        PriceSubscriptionApi.PriceSubscriptionRequest priceSubscriptionRequest = new PriceSubscriptionApi.PriceSubscriptionRequest(contextId, referenceId, uic, assetType);

        Client client = ClientBuilder.newClient();
        Response response = client.target(subscriptionUri)
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + atomicOAuthToken.get().accessToken)
                .post(Entity.json(priceSubscriptionRequest));

        switch (response.getStatus()) {
            case 201:
                PriceSubscriptionApi.PriceSubscriptionResponse priceSubscriptionResponse = response.readEntity(PriceSubscriptionApi.PriceSubscriptionResponse.class);
                System.out.println("OK, response=" + Jsons.toString(priceSubscriptionResponse));
                return priceSubscriptionResponse;
            default:
                throw new IllegalStateException("Unexpected response status: " + response.getStatus());
        }
    }

    public void unsubscribe(String contextId, String referenceId) {
        System.out.println("unsubscribe():");
        System.out.println("  contextId=" + contextId);
        System.out.println("  referenceId=" + referenceId);

        Client client = ClientBuilder.newClient();
        Response response = client.target(subscriptionUri).path(contextId).path(referenceId).request().delete();

        System.out.println("delete status=" + response.getStatus());

        switch (response.getStatus()) {
            case 200:
                return;
            default:
                throw new IllegalStateException("Unexpected response status: " + response.getStatus());
        }
    }

    public void unsubscribe(String contextId, String... referenceIds) {
        if (referenceIds != null) {
            for (String referenceId : referenceIds) {
                unsubscribe(contextId, referenceId);
            }
        }
    }
}