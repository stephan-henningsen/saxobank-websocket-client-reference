package com.saxobank.reference.websocketapp;


import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class WebSocketClientEndpoint extends Endpoint {

    public interface WebSocketClientEndpointListener {
        void onWebSocketClientEndpointOpen();
        void onWebSocketClientEndpointClose(CloseReason closeReason);
        void onWebSocketClientEndpointError(Throwable thr);
    }



    private final String connectionUri;
    private final String contextId;
    private final AtomicReference<OAuthToken> atomicOAuthToken;
    private WebSocketClientEndpointListener listener;
    private WebSocketMessageHandler messageHandler;

    private Session session = null;



    public WebSocketClientEndpoint(String connectionUri, String contextId, AtomicReference<OAuthToken> atomicOAuthToken, WebSocketClientEndpointListener listener, WebSocketMessageHandler messageHandler) {
        this.connectionUri = connectionUri;
        this.contextId = contextId;
        this.atomicOAuthToken = atomicOAuthToken;
        this.listener = listener;
        this.messageHandler = messageHandler;
    }



    public void connect() throws IOException, DeploymentException {
        connect(-1);
    }

    public void connect(long lastReceivedMessageId) throws IOException, DeploymentException {
        URI connectionUri;
        if (lastReceivedMessageId == -1) {
            connectionUri = URI.create(String.format("%s?contextId=%s", this.connectionUri, contextId));
        } else {
            connectionUri = URI.create(String.format("%s?contextId=%s&messageId=%s", this.connectionUri, contextId, lastReceivedMessageId));
        }

        ClientEndpointConfig.Builder endpointConfigBuilder = ClientEndpointConfig.Builder.create();
        endpointConfigBuilder.configurator(new ClientEndpointConfig.Configurator() {
            @Override
            public void beforeRequest(Map<String, List<String>> headers) {
                headers.put("Authorization", Collections.singletonList("Bearer " + atomicOAuthToken.get().accessToken));
            }
        });
        ClientEndpointConfig clientEndpointConfig = endpointConfigBuilder.build();

        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        container.connectToServer(this, clientEndpointConfig, connectionUri);
    }

    public void close() {
        System.out.println("close()");
        if (session != null && session.isOpen()) {
            try {
                session.close();
            } catch (IOException ex) {
                System.err.println("Ouch, failed closing session");
                ex.printStackTrace();
            }
        }
    }


    @Override
    public void onOpen(Session session, EndpointConfig config) {
        System.out.println("onOpen()");
        this.session = session;
        this.session.addMessageHandler(messageHandler);
        if ( listener != null ) {
            listener.onWebSocketClientEndpointOpen();
        }
    }

    @Override
    public void onClose(Session session, CloseReason closeReason) {
        System.out.println("onClose(), closeReason=" + closeReason.getCloseCode());
        if ( listener != null ) {
            listener.onWebSocketClientEndpointClose(closeReason);
        }
    }

    @Override
    public void onError(Session session, Throwable thr) {
        System.err.println("onError()");
        if ( listener != null ) {
            listener.onWebSocketClientEndpointError(thr);
        }
    }

}
