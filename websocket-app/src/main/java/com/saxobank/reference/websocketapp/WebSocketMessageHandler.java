package com.saxobank.reference.websocketapp;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.websocket.MessageHandler;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class WebSocketMessageHandler implements MessageHandler.Whole<byte[]> {

    private static int MESSAGE_ID_LENGTH = 8;
    private static int ENVELOPE_VERSION_LENGTH = 2;
    private static int REFERENCE_ID_SIZE_LENGTH = 1;
    private static int PAYLOAD_FORMAT_LENGTH = 1;
    private static int PAYLOAD_SIZE_LENGTH = 4;


    public interface OnMessageReceivedListener {
        enum PayloadFormat {Json, ProtoBuffer}

        @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
        class Message {
            public long messageId;
            public String referenceId;
            public PayloadFormat payloadFormat;
            byte[] payload;

            protected Message() {
            }

            public Message(long messageId, String referenceId, PayloadFormat payloadFormat, byte[] payload) {
                this.messageId = messageId;
                this.referenceId = referenceId;
                this.payloadFormat = payloadFormat;
                this.payload = payload;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Message that = (Message)o;
                return this.messageId == that.messageId &&
                        Objects.equals(this.referenceId, that.referenceId) &&
                        Objects.equals(this.payloadFormat, that.payloadFormat) &&
                        Arrays.equals(this.payload, that.payload);
            }

            @Override
            public int hashCode() {
                return Objects.hash(messageId, referenceId, payloadFormat, payload);
            }
        }

        @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
        class HeartbeatControlMessage {
            public String referenceId;
            public Heartbeat[] heartbeats;
        }

        @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
        class Heartbeat {
            public String originatingReferenceId;
            public String reason;
        }


        @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
        class ResetSubscriptionsControlMessage {
            public String referenceId;
            public String[] targetReferenceIds;
        }

        @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
        class DisconnectControlMessage {
            public String referenceId;
        }

        void onMessage(Message message);

        void onHeartbeat(HeartbeatControlMessage[] controlMessages);

        void onResetSubscription(ResetSubscriptionsControlMessage[] controlMessages);

        void onDisconnect(DisconnectControlMessage[] controlMessages);
    }


    private List<OnMessageReceivedListener> listeners = new ArrayList<>();


    /**
    *   ,----------------------+-------+--------+-------------------------------------------------.
    *   | Field                | Bytes | Type   | Note                                            |
    *   +----------------------+-------+--------+-------------------------------------------------+
    *   | Message Id           | 8     | Int64  | 64-bit little-endian unsigned integer.          |
    *   | Envelope Version     | 2     | Int16  | Always 0.                                       |
    *   | Reference Id Size    | 1     | Int8   | 8-bit little-endian unsigned integer.           |
    *   | Reference Id         | n     | Ascii  | n = Reference Id Size. String is ASCII encoded. |
    *   | Payload Format       | 1     | Int8   | 0 = Json. 8-bit unsigned integer.               |
    *   | Payload Size         | 4     | Int32  | 32-bit little-endian unsigned integer.          |
    *   | Payload              | n     | Byte[] | n = Payload Size. Json is UTF8 encoded.         |
    *   `----------------------+-------+--------+-------------------------------------------------'
    **/
    @Override
    public void onMessage(byte[] messageByteArray) {
        System.out.print("onMessage(), " + messageByteArray.length + " bytes");

        List<OnMessageReceivedListener.Message> messages = parseMessages(messageByteArray);
        System.out.println(", parsed "+messages.size()+" Message(s)");

        for (OnMessageReceivedListener.Message message : messages) {
            processMessage(message);
        }
    }


    private List<OnMessageReceivedListener.Message> parseMessages(byte[] messageByteArray) {
        final ByteBuffer byteBuffer = ByteBuffer.wrap(messageByteArray).order(ByteOrder.LITTLE_ENDIAN);
        final List<OnMessageReceivedListener.Message> messages = new ArrayList<>();

        int index = 0;

        while ( index < messageByteArray.length ) {
            long messageId = byteBuffer.getLong(index);
            index += MESSAGE_ID_LENGTH; // Advance index point past Message Id.

            index += ENVELOPE_VERSION_LENGTH; // Skip the Envelope Version field.

            byte referenceIdSize = byteBuffer.get(index);
            byte[] referenceIdByteArray = new byte[referenceIdSize];
            index += REFERENCE_ID_SIZE_LENGTH;

            System.arraycopy(byteBuffer.array(), index, referenceIdByteArray, 0, referenceIdByteArray.length);
            String referenceId = new String(referenceIdByteArray, StandardCharsets.US_ASCII);
            index += referenceIdSize;

            byte payloadFormatIndex = byteBuffer.get(index);
            OnMessageReceivedListener.PayloadFormat payloadFormat = OnMessageReceivedListener.PayloadFormat.values()[payloadFormatIndex];
            index += PAYLOAD_FORMAT_LENGTH;

            int payloadSize = byteBuffer.getInt(index);
            index += PAYLOAD_SIZE_LENGTH;

            byte[] payloadByteArray = new byte[payloadSize];
            System.arraycopy(byteBuffer.array(), index, payloadByteArray, 0, payloadSize);
            index += payloadSize;

            OnMessageReceivedListener.Message message = new OnMessageReceivedListener.Message(messageId, referenceId, payloadFormat, payloadByteArray);
            messages.add(message);
        }

        return messages;
    }

    private void processMessage ( OnMessageReceivedListener.Message message )
    {
        if (message.referenceId.startsWith("_")) {
            try {
                switch (message.referenceId) {
                    case "_heartbeat":
                        OnMessageReceivedListener.HeartbeatControlMessage[] heartbeatControlMessages = Jsons.toObject(message.payload, OnMessageReceivedListener.HeartbeatControlMessage[].class);
                        notifyOnHeartbeat(heartbeatControlMessages);
                        break;

                    case "_resetsubscriptions":
                        OnMessageReceivedListener.ResetSubscriptionsControlMessage[] resetSubscriptionsControlMessages = Jsons.toObject(message.payload, OnMessageReceivedListener.ResetSubscriptionsControlMessage[].class);
                        notifyOnResetSubscription(resetSubscriptionsControlMessages);
                        break;

                    case "_disconnect":
                        OnMessageReceivedListener.DisconnectControlMessage[] disconnectControlMessages = Jsons.toObject(message.payload, OnMessageReceivedListener.DisconnectControlMessage[].class);
                        notifyOnDisconnect(disconnectControlMessages);
                        break;

                    default:
                        throw new IllegalArgumentException("Unexpected Control Message ReferenceId: " + message.referenceId);
                }
            } catch (IOException ex) {
                throw new IllegalArgumentException("Unexpected Message Payload, referenceId=" + message.referenceId, ex);
            }
        } else {
            notifyOnMessage(message);
        }
    }


    public void addOnMessageReceivedListener(OnMessageReceivedListener listener) {
        listeners.add(listener);
    }

    public void removeOnMessageReceivedListener(OnMessageReceivedListener listener) {
        listeners.remove(listener);
    }


    protected void notifyOnMessage(OnMessageReceivedListener.Message message) {
        for (OnMessageReceivedListener listener : listeners) {
            listener.onMessage(message);
        }
    }

    protected void notifyOnHeartbeat(OnMessageReceivedListener.HeartbeatControlMessage[] controlMessages) {
        for (OnMessageReceivedListener listener : listeners) {
            listener.onHeartbeat(controlMessages);
        }
    }

    protected void notifyOnResetSubscription(OnMessageReceivedListener.ResetSubscriptionsControlMessage[] controlMessages) {
        for (OnMessageReceivedListener listener : listeners) {
            listener.onResetSubscription(controlMessages);
        }
    }

    protected void notifyOnDisconnect(OnMessageReceivedListener.DisconnectControlMessage[] controlMessages) {
        for (OnMessageReceivedListener listener : listeners) {
            listener.onDisconnect(controlMessages);
        }
    }

}
