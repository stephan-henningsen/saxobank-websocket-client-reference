package com.saxobank.reference.websocketapp;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class AuthorizationClient {

    private final URI authorizationUri;

    public AuthorizationClient(String authorizationUrl) {
        authorizationUri = URI.create(authorizationUrl);
    }

    public void reauthorize (String authToken, String contextId )
    {
        System.out.println("Reauthorizing...");

        URI reauthorizeUri = UriBuilder.fromUri(authorizationUri).queryParam("contextid", contextId).build();

        Response response = ClientBuilder.newClient().target(reauthorizeUri)
                .request()
                .header("Authorization", "Bearer " + authToken)
                .put(Entity.json(""));

        switch (response.getStatus()) {
            case 202:
                System.out.println("Ok, reauthorized");
                break;
            default:
                throw new IllegalStateException("Unexpected response status: " + response.getStatus());
        }
    }

}