package com.saxobank.reference.websocketapp;

import org.junit.Test;
import org.mockito.Mockito;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class WebSocketMessageHandlerTest {

    @Test
    public void parseMessage() {

        // Arrange
        final long messageId = 0xffffffff;
        final String referenceId = "12345"; // 5 bytes
        final WebSocketMessageHandler.OnMessageReceivedListener.PayloadFormat payloadFormat = WebSocketMessageHandler.OnMessageReceivedListener.PayloadFormat.Json;
        final byte[] payload = "abcdefg".getBytes(); // 7 bytes.

        final byte[] messageByteArray = createMessageByteArray(messageId, referenceId, payloadFormat, payload);
        final WebSocketMessageHandler.OnMessageReceivedListener.Message message = new WebSocketMessageHandler.OnMessageReceivedListener.Message(messageId, referenceId, payloadFormat, payload);

        WebSocketMessageHandler.OnMessageReceivedListener mockedOnMessageReceivedListener = Mockito.mock(WebSocketMessageHandler.OnMessageReceivedListener.class);
        WebSocketMessageHandler messageHandler = new WebSocketMessageHandler();
        messageHandler.addOnMessageReceivedListener(mockedOnMessageReceivedListener);

        // Act.

        messageHandler.onMessage(messageByteArray);


        // Assert.

        Mockito.verify(mockedOnMessageReceivedListener).onMessage(message);
    }


    /**
    * ,----------------------+-------+--------+-------------------------------------------------.
    * | Field                | Bytes | Type   | Note                                            |
    * +----------------------+-------+--------+-------------------------------------------------+
    * | Message Id	         | 8     | Int64  | 64-bit little-endian unsigned integer.          |
    * | Envelope Version     | 2     | Int16  | Always 0.                                       |
    * | Reference Id Size    | 1     | Int8   | 8-bit little-endian unsigned integer.           |
    * | Reference Id         | n     | Ascii  | n = Reference Id Size. String is ASCII encoded. |
    * | Payload Format       | 1     | Int8   | 0 = Json. 8-bit unsigned integer.               |
    * | Payload Size         | 4     | Int32  | 32-bit little-endian unsigned integer.          |
    * | Payload              | n     | Byte[] | n = Payload Size. Json is UTF8 encoded.         |
    * `----------------------+-------+--------+-------------------------------------------------'
    **/
    private static byte[] createMessageByteArray(long messageId, String referenceId, WebSocketMessageHandler.OnMessageReceivedListener.PayloadFormat payloadFormat, byte[] payload) {
        ByteBuffer buf = ByteBuffer.wrap(new byte[1024]).order(ByteOrder.LITTLE_ENDIAN);

        buf.putLong(messageId);
        buf.put((byte)0).put((byte)0);
        buf.put((byte) referenceId.length());
        buf.put(referenceId.getBytes());
        buf.put((byte) payloadFormat.ordinal());
        buf.putInt(payload.length);
        buf.put(payload);

        int length = buf.position();

        byte[] message = new byte[length];
        System.arraycopy(buf.array(), 0, message, 0, length);

        return message;
    }

}
