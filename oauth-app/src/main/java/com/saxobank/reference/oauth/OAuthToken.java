
package com.saxobank.reference.oauth;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.LowerCaseWithUnderscoresStrategy.class)
public class OAuthToken {


    public static OAuthToken create(String authToken, int expiresIn, String tokenType)
    {
        return new OAuthToken(authToken, expiresIn, tokenType, "example-refresh-token", 12345);
    }



    public String accessToken;
    public int expiresIn;
    public String tokenType;
    public String refreshToken;
    public int refreshTokenExpiresIn;

    protected OAuthToken() {
    }

    public OAuthToken(String accessToken, int expiresIn, String tokenType, String refreshToken, int refreshTokenExpiresIn) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.tokenType = tokenType;
        this.refreshToken = refreshToken;
        this.refreshTokenExpiresIn = refreshTokenExpiresIn;
    }


}