package com.saxobank.reference.oauth;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Implementation to authenticate against the SaxoBank OpenAPI,
 * https://www.developer.saxo/openapi/learn/OAuth-2-0-Authentication
 **/
public class OAuthClient {

    public static OAuthClient create()
    {
        return new OAuthClient(OAuthApp.AUTHENTICATION_URL, OAuthApp.APP_URL, OAuthApp.APP_KEY, OAuthApp.APP_SECRET);
    }

    public class OAuthClientException extends Exception {
        public OAuthClientException(Exception ex) {
            super(ex);
        }

        public OAuthClientException(String message) {
            super(message);
        }

        public OAuthClientException(String message, Exception ex) {
            super(message, ex);
        }
    }


    private final String authenticationBaseUrl;
    private final String appUrl;
    private final String appKey;
    private final String appSecret;



    public OAuthClient(String authenticationBaseUrl, String appUrl, String appKey, String appSecret) {
        this.authenticationBaseUrl = authenticationBaseUrl;
        this.appUrl = appUrl;
        this.appKey = appKey;
        this.appSecret = appSecret;
    }

    public OAuthToken authenticate() throws OAuthClientException {
        String context = "context-" + UUID.randomUUID();

        String code = getCode(context, appKey, appUrl);
        OAuthToken authToken = getToken(context, code, appKey, appSecret, appUrl);

        return authToken;
    }

    public Future<OAuthToken> authenticateFuture() {
        CompletableFuture<OAuthToken> completableFuture = new CompletableFuture<>();

        Executors.newCachedThreadPool().submit(() -> {
            try {
                OAuthToken authToken = authenticate();
                completableFuture.complete(authToken);
            } catch (OAuthClientException ex) {
                completableFuture.completeExceptionally(ex);
            }
            return null;
        });

        return completableFuture;
    }

    public Future<OAuthToken> authenticateFuture(String overrideAuthToken) {
        CompletableFuture<OAuthToken> completableFuture = new CompletableFuture<>();

        Executors.newCachedThreadPool().submit(() -> {
            try {
                OAuthToken authToken = OAuthToken.create(overrideAuthToken, 1000*2*60, "future-initial-auth-token");
                Thread.sleep(2000);
                completableFuture.complete(authToken);
            } catch (InterruptedException ex) {
                completableFuture.completeExceptionally(ex);
            }
            return null;
        });

        return completableFuture;
    }




    private String getCode(String context, String appKey, String appUrl) throws OAuthClientException {
        URI authorizeUri = UriBuilder.fromPath(authenticationBaseUrl)
                .path("authorize")
                .queryParam("response_type", "code")
                .queryParam("client_id", appKey)
                .queryParam("state", context)
                .queryParam("redirect_uri", appUrl)
                .build();

        Response response = ClientBuilder.newClient()
                .target(authorizeUri)
                .request(MediaType.APPLICATION_FORM_URLENCODED)
                .get();

        switch (response.getStatus()) {
            case 302:
                try {
                    String locationUrl = response.getHeaderString("Location");
                    String code = Urls.getQueryParamValue("code", locationUrl);
                    String requestId = Urls.getQueryParamValue("requestId", locationUrl);
                    return requestId;
                } catch (UnsupportedEncodingException ex) {
                    throw new OAuthClientException("Error parsing Location URL", ex);
                }
            default:
                throw new OAuthClientException("Unexpected result status: " + response.getStatus());
        }
    }


    private OAuthToken getToken(String context, String code, String appKey, String appSecret, String appUrl) throws OAuthClientException {
        URI tokenUri = UriBuilder.fromPath(authenticationBaseUrl).path("token").build();

        Form requestForm = new Form();
        requestForm.param("grant_type", "authorization_code");
        requestForm.param("code", code);
        requestForm.param("redirect_uri", appUrl);
        requestForm.param("client_id", appKey);
        requestForm.param("client_secret", appSecret);

        Response response = ClientBuilder.newClient()
                .target(tokenUri)
                .request()
                .post(Entity.form(requestForm));

        switch (response.getStatus()) {
            case 201:
                OAuthToken authToken = response.readEntity(OAuthToken.class);
                return authToken;
            default:
                throw new OAuthClientException("Unexpected result status: " + response.getStatus());
        }
    }


}
