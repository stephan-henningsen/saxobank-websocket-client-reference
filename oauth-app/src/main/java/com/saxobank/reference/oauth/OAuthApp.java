package com.saxobank.reference.oauth;

public class OAuthApp {

    static final String APP_KEY = "24fcdf27d020440182c144f2ec125bb7";
    static final String APP_SECRET = "b80a6e40f5804025ab329ef9f39bb187";
    static final String APP_URL = "http://localhost/bjotradersim";

    static final String AUTHENTICATION_URL = "https://sim.logonvalidation.net";
    static final String OPEN_API_BASE_URL = "https://gateway.saxobank.com/sim/openapi";
    static final String PARTNER_ID_URL = "https://sim.logonvalidation.net";



    public static void main(String[] args) {
        new OAuthApp().run();
    }



    private void run() {
        OAuthClient client = new OAuthClient(AUTHENTICATION_URL, APP_URL, APP_KEY, APP_SECRET);

        try {
            OAuthToken authToken = client.authenticate();
            System.out.println("Got OAuthToken: "+Jsons.toString(authToken));
        } catch (OAuthClient.OAuthClientException ex) {
            ex.printStackTrace();
        }
    }


}
