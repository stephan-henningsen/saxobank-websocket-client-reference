package com.saxobank.reference.oauth;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class Urls {

    public static String getQueryParamValue ( String queryParamKey, String url ) throws UnsupportedEncodingException {
        String query = url.substring( url.indexOf("?")+1 );
        String[] queryKeyValues = query.split("&");
        for ( String queryKeyValue : queryKeyValues ) {
            int x = queryKeyValue.indexOf("=");
            String key = URLDecoder.decode(queryKeyValue.substring(0, x), "UTF-8");

            if ( key.equals(queryParamKey) ) {
                String value =  URLDecoder.decode(queryKeyValue.substring(x + 1), "UTF-8");
                return value;
            }
        }
        return null;
    }

}
