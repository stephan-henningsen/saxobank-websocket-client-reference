package com.saxobank.reference.oauth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Objects;

public class Jsons {

    public static String toString(Object object) {
        try {
            if (object == null) {
                return Objects.toString(object);
            } else {
                return new ObjectMapper().writeValueAsString(object);
            }
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static <T> T toObject(byte[] bytes, Class<T> type, T defaultObject) {
        try {
            return new ObjectMapper().readValue(bytes, type);
        } catch (IOException ex) {
            return defaultObject;
        }
    }

    public static <T> T toObject(byte[] bytes, Class<T> type) throws IOException {
        return new ObjectMapper().readValue(bytes, type);
    }

}
