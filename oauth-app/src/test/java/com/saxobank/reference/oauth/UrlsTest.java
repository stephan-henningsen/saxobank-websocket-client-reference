package com.saxobank.reference.oauth;

import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;

public class UrlsTest {

    @Test
    public void queryParamExtraction() throws UnsupportedEncodingException {
        // Arrange.
        String url = "http://example.com/?key1=value1&key2=value2&key3=value3";

        // Act.
        String actualValue1 = Urls.getQueryParamValue("key1", url);
        String actualValue2 = Urls.getQueryParamValue("key2", url);
        String actualValue3 = Urls.getQueryParamValue("key3", url);
        String actualNonExistingValue = Urls.getQueryParamValue("non-existing-ke7", url);

        // Assert.
        Assert.assertEquals("value1", actualValue1);
        Assert.assertEquals("value2", actualValue2);
        Assert.assertEquals("value3", actualValue3);
        Assert.assertEquals(null, actualNonExistingValue);
    }
}
