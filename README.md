

# Overview #

Here are two simple, text-based reference implementations in Java for the SaxoBank OpenAPI.

1. The **oauth-app** will perform OAuth authorization.

1. The **websocket-app** will subscribe to pricing events and receive updates over the WebSocket and display them.  It will also perform initial authentication and periodically reauthenticate.

It is advise that all commands in this document are copied and pasted into the shell as opposed to trying to type them by hand.

## Running the apps ##

There are several methods for running the apps.

1. Running the app jar. This method requires that JDK8 8 is installed and will compile sources and pack them into an *exectuable fat jar*, that is, a java archive that has the `Main-Class` set in its `MANIFEST.MF`, and contains all class files from the app along with its dependencies.
2. Running the app in Docker. This requires that Docker Enigne is installed and will basically do the same as above, but without within a container; no need for a local JDK.

Press Ctrl+C to stop a running app.

# The oauth-app #

## Running the oauth-app jar ##

1. Compile the app:

    `./gradlew websocket-app:fatJar`

2. Run the app:

    `java -jar websocket-app/build/libs/websocket-app-fat.jar`

## Running the oauth-app from Docker ##

1. Build the container for the app:

    `docker build . -f Dockerfile-oauth-app -t oauth-app`

2. Run the container:

    `docker run --rm -it oauth-app`



# The websocket-app #

Prior to running the websocket-app, a valid authorization token has to be acquired from the SaxoBank Developer Portal and fed to the app though the `AUTH_TOKEN` environment variable.

If none is supplied, the app will attempt to acquire one from the server.


## Running the websocket-app jar ##

0. Supply a valid authorization token:

    `export AUTH_TOKEN="eyJhbGciOiZta..."`

1. Compile the app:

    `./gradlew websocket-app:fatJar`

2. Run the app:

    `java -jar websocket-app/build/libs/websocket-app-fat.jar`

## Running the websocket-app from Docker ##

0. Supply a valid authorization token:

    `export AUTH_TOKEN="eyJhbGciOiZta..."`

1. Build the container for the app:

    `docker build . -f Dockerfile-websocket-app -t websocket-app`

2. Run the container:

    `docker run --rm -e AUTH_TOKEN=$AUTH_TOKEN -it websocket-app`


